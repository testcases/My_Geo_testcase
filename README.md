### Welcome!

This is the test case for %company_name%.

### For installation:

- git clone https://gitlab.com/Artem.H/My_Geo_testcase.git
- pip install -r requirements.txt

### Notes

Use only python >= 3.9!

Run such as specified in the task - either comma-separated city names, or key 
and file name.

For API requests uses service https://www.back4app.com/

To add new fields to the response - you need to add field changes to the file 
fields.txt. If the connected API can provide it, they will be displayed.  
In this way I tried to solve the problem 
of adding fields without changing the code.

I provided error messages in the displaying, if exception are occurred.

```
Error
----------
Invalid filename
----------


Error
----------
System Error. Please contact support. 'ArgsParser' object has no attribute 'citiesx'
----------

```

### Files

- My_Geo.py - Main file with CLI interface 
 
- file tools.py - File contains basic classes
    - ArgsParser
    - DefaultApiReq
    - DefaultSerializer
    
- TASK.md - Original task 

- cities.txt - Contains city names

- fields.txt - Contains additional fields for displaying

- requirements.txt - installation requirements

