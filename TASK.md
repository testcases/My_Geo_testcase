### Original task

The task is to develop the GeoInsightFetcher CLI tool.

The My_Geo is a tool that helps users to get some insights about cities in the world.
When the GeoInsightFetcher gets a name of a city, it returns some interesting insights about it.
The project requirements are as follows:

● The project should be written in Python.

● All of the examples given in task_examples.md should return the same result (different
formatting is allowed).

● The minimum insights that the My_Geo should return is Country and Currency,
but you can add more.

● All the data of My_Geo should be retrieved using an external source (API).

● The My_Geo should be built in the way, so it’s easy to add another field (for
example, city population growth) with minimum changes in code.

● The project should be integrated with Git VCS, and hosted on github or another similar
hosting provider.

● Virtual Environment should be used.

● A readme.md file should be attached to the project repository explaining how to build the
project from scratch and run it.

● A CLI should be also built in order to test the GeoInsightFetcher.

● You DO NOT need to handle exotic inputs that don’t make sense, you can assume some
stuff on the input type and write it in the readme.md. Do not waste time on excessive
input validation.

Bonus task - Allow the fetcher to accept a file as an input (file contains an example).

#### Example

---

**python My_Geo.py tel aviv**
```
=> Tel Aviv
=>-------------
=>Country: Israel
=>Currency: NIS
=>-------------
```
---

**python My_Geo.py tEl aViv, kiev**
```
=> Tel Aviv
=>-------------
=>Country: Israel
=>Currency: NIS
=>-------------
=>
=> Kiev
=>-------------
=>Country: Ukraine
=>Currency: HRY
=>-------------
```
---
**python My_Geo.py asdasd**
```
=> asdasd
=>-------------
=> Invalid City Name
=>-------------
```
---

**python My_Geo.py asdasd, kiEv**
```
=> asdasd
=>-------------
=> Invalid City Name
=>-------------
=>
=>  Kiev
=>-------------
=>Country: Ukraine
=>Currency: HRY
=>-------------
```
---

## Bonus

**python My_Geo.py -f cities.txt**
```
=>  Tel Aviv
=>-------------
=>Country:  Israel
=>Currency: NIS
=>-------------
=>
=>  Kiev
=>-------------
=>Country: Ukraine
=>Currency: HRY
=>-------------
=>  New York
=>-------------
=>Country: USA
=>Currency: USD
=>-------------

```

cities.txt
```
Tel aviv
Kiev
New York
```